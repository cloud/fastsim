FROM tensorflow/tensorflow:nightly-gpu-py3-jupyter
#FROM tensorflow/tensorflow:1.13.2-gpu-jupyter
SHELL ["/bin/bash", "-c"]

ADD data /data

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
	git \
	sudo \
	vim \
	cmake \
	dpkg-dev \
	python-dev \ 
	make \
	g++ \
	gcc \
	binutils \
	libx11-dev \
	libxpm-dev \
	libxft-dev \
	libxext-dev 
	#gfortran \
	#libssl-dev \
	#libpcre3-dev \
	#xlibmesa-glu-dev \
	#libglew1.5-dev \ 
	#libftgl-dev \ 
	#libmysqlclient-dev \ 
	#libfftw3-dev \
	#graphviz-dev \
	#libavahi-compat-libdnssd-dev \
	#libldap2-dev \
	#libxml2-dev \
	#libkrb5-dev \
	#libgsl0-dev \
	#libqt4-dev \
	

RUN cd /usr/local/ && \
	sudo git clone --depth 1 https://github.com/root-mirror/root.git && \
	sudo chown -R $(whoami):$(id -g -n $(whoami)) root && \
	cd root && \
	mkdir compile && \
	cd compile && \
	cmake -Dpython=ON -Droofit=ON -DCMAKE_INSTALL_PREFIX:PATH=/usr/local/root .. && \
	make -j$(nproc) && \
	sudo make install && \
	cd /usr/local/root && \
	source bin/thisroot.sh && \ 
	echo "#CERN ROOT" >> ~/.bashrc && \
	echo "export PATH=$ROOTSYS/bin:$PATH" >> ~/.bashrc && \
	echo "export PYTHONDIR=$ROOTSYS" >> ~/.bashrc && \
	echo "export LD_LIBRARY_PATH=$ROOTSYS/lib:$PYTHONDIR/lib:$ROOTSYS/bindings/pyroot:$LD_LIBRARY_PATH" >> ~/.bashrc && \
	echo "export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH:$ROOTSYS/bindings/pyroot" >> ~/.bashrc 
RUN pwd
RUN /bin/bash -c "source ~/.bashrc"

#RUN git clone -b Anglegan https://github.com/svalleco/3Dgan.git

RUN git clone -b Anglegan https://github.com/owoshch/3Dgan.git


RUN pip install \
        keras==2.2.4 \
        matplotlib \
        scikit-learn \
        uproot \
        jupyter \
        jupyterhub \
        jupyterlab \
        matplotlib \
        seaborn \
        hep_ml \
        tables \
        papermill pydot Pillow


RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user
